// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.utils;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotContainer;

public class PathChooser {

    public static final String TAB_NAME = "Path Chooser"; // name of the chooser tab

    private SendableChooser<CommandBase> choosers; // all the choosers

    public PathChooser(RobotContainer container){

        choosers = new SendableChooser<>();

        choosers.addOption("Trench Run #1", container.getTrenchRunAuto1());
        choosers.addOption("Trench Run #2", container.getTrenchRunAuto2());
        choosers.addOption("Center", container.getCenterAuto());
        choosers.addOption("Opponent Trench Run", container.getOpponentTrenchAuto());
        choosers.addOption("Pussy Mode", null);

        SmartDashboard.putData(TAB_NAME, choosers);

    }

    public CommandBase getSelectedAuto(){
        return choosers.getSelected();
    }

}
