// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.Constants.DriveConstants;
import frc.robot.subsystems.DrivingSubsystem;

/** Add your docs here. */
public class TrajectoryHandler {

    private DifferentialDriveVoltageConstraint voltageConstraint;
    private DrivingSubsystem drivingSubsystem;

    private HashMap<String, Trajectory> trajectories;

    public TrajectoryHandler(DrivingSubsystem drivingSubsystem){

        this.drivingSubsystem = drivingSubsystem;

        voltageConstraint = new DifferentialDriveVoltageConstraint(
            new SimpleMotorFeedforward(DriveConstants.ksVolts,
                                       DriveConstants.kvVoltSecondsPerMeter,
                                       DriveConstants.kaVoltSecondsSquaredPerMeter),
            drivingSubsystem.getDriveKinematics(),
            DriveConstants.MAX_VOLTAGE);

        
        initializeTrajectories();

    }

    private void initializeTrajectories(){
        trajectories = new HashMap<>();

    }

    public Trajectory loadTrajectory(String path){
        String trajectoryJSON = "paths/" + path + ".wpilib.json";
        Trajectory trajectory = new Trajectory();
        try {
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(trajectoryJSON);
            trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
        } catch (IOException ex) {
            DriverStation.reportError("Unable to open trajectory: " + trajectoryJSON, ex.getStackTrace());
        }

        return trajectory;
    }

    public RamseteCommand getPathDriveCommand(String pathName){
        Trajectory trajectory = trajectories.get(pathName);

        RamseteCommand ramseteCommand = new RamseteCommand(
        trajectory,
        drivingSubsystem::getPose,
        new RamseteController(AutoConstants.kRamseteB, AutoConstants.kRamseteZeta),
        new SimpleMotorFeedforward(DriveConstants.ksVolts,
                                   DriveConstants.kvVoltSecondsPerMeter,
                                   DriveConstants.kaVoltSecondsSquaredPerMeter),
        drivingSubsystem.getDriveKinematics(),
        drivingSubsystem::getWheelSpeeds,
        new PIDController(DriveConstants.kPDriveVel, 0, 0),
        new PIDController(DriveConstants.kPDriveVel, 0, 0),
        // RamseteCommand passes volts to the callback
        drivingSubsystem::driveVoltage,
        drivingSubsystem
        );

        return ramseteCommand;
    }

}
