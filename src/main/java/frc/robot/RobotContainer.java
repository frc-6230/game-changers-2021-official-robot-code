// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;


import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.JoystickConstants;
import frc.robot.Utilities.Vision;
import frc.robot.commands.auto.DriveByMetersCommand;
import frc.robot.commands.auto.TurnAndShootCommand;
import frc.robot.commands.auto.TurnAndShootWithoutDelivery;
import frc.robot.commands.auto.TurnInPlaceCommand;
import frc.robot.commands.auto.TurnToTargetCommand;
import frc.robot.commands.collecting.CloseCollectorCommand;
import frc.robot.commands.collecting.CollectCommand;
import frc.robot.commands.collecting.OpenCollectorCommand;
import frc.robot.commands.delivery.LoadBallsCommand;
import frc.robot.commands.shooting.LoadAndShootCommand;
import frc.robot.commands.shooting.ShootByRpm;
import frc.robot.subsystems.AngleChangerSubsystem;
import frc.robot.subsystems.CollectionSubsystem;
import frc.robot.subsystems.DeliverySubsystem;
import frc.robot.subsystems.DrivingSubsystem;
import frc.robot.subsystems.ShootingSubsystem;
import frc.robot.utils.TrajectoryHandler;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {

  private TrajectoryHandler trajectoryHandler;

  public DrivingSubsystem drivingSubsystem;
  public CollectionSubsystem collectionSubsystem;
  public DeliverySubsystem deliverySubsystem;
  public ShootingSubsystem shootingSubsystem;
  public AngleChangerSubsystem angleChangerSubsystem;

  public CollectCommand collectCommand;
  public OpenCollectorCommand openCollectorCommand;
  public CloseCollectorCommand closeCollectorCommand;
  public TurnAndShootWithoutDelivery turnAndShootWithoutDelivery;
  public LoadAndShootCommand loadAndShootCommand;

  public LoadBallsCommand loadBallsCommand;
  public ShootByRpm shootByRpm;
  public CommandBase OpenAndCollect;
  public CommandBase turnToTargetAndShoot;
  public CommandBase loadAndShoot;
  public CommandBase loadBalls;
  public CommandBase shoot;
  private Joystick joystick = new Joystick(Constants.JoystickConstants.JOYSTICK_BUTTON1);

  public RobotContainer() {
    initializeSubsystems();
    initializeCommands();
    configureButtonBindings();
    trajectoryHandler = new TrajectoryHandler(drivingSubsystem);

  }
  public void initializeSubsystems(){
    drivingSubsystem = new DrivingSubsystem();
    collectionSubsystem = new CollectionSubsystem();
    shootingSubsystem = new ShootingSubsystem();
    deliverySubsystem = new DeliverySubsystem();
    angleChangerSubsystem = new AngleChangerSubsystem();
  }

  public void initializeCommands(){
    collectCommand = new CollectCommand(collectionSubsystem);
    closeCollectorCommand = new CloseCollectorCommand(collectionSubsystem);
    openCollectorCommand = new OpenCollectorCommand(collectionSubsystem);
    loadBallsCommand = new LoadBallsCommand(deliverySubsystem);
    shootByRpm = new ShootByRpm(shootingSubsystem);
    OpenAndCollect = openCollectorCommand.andThen(collectCommand);  
    turnAndShootWithoutDelivery = new TurnAndShootWithoutDelivery(this, Vision.getShootAngle());
    loadAndShoot = new LoadAndShootCommand(shootingSubsystem, deliverySubsystem);
    turnToTargetAndShoot = new TurnAndShootCommand(this,Vision.getShootAngle());
  }



  private void configureButtonBindings() {
    JoystickButton openAndCollectButton = new JoystickButton(joystick, Constants.JoystickConstants.OPEN_AND_COLLECT_BUTTON);
    JoystickButton turnToTargetAndShootButton = new JoystickButton(joystick, Constants.JoystickConstants.JOYSTICK_BUTTON1);
    JoystickButton loadAndShootButton = new JoystickButton(joystick, Constants.JoystickConstants.LOAD_AND_SHOOT_BUTTON);
    JoystickButton loadballsButton = new JoystickButton(joystick, Constants.JoystickConstants.LOAD_BALLS_BUTTON);
    JoystickButton shootButton = new JoystickButton(joystick, Constants.JoystickConstants.SHOOT_BUTTON);
    openAndCollectButton.whenHeld(OpenAndCollect);
    turnToTargetAndShootButton.whenPressed(turnToTargetAndShoot);
    loadAndShootButton.whenPressed(loadAndShoot);
    loadballsButton.whenPressed(loadBalls);
    shootButton.whenPressed(shoot);

    


  }

  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return null;
  }

  public CommandBase getTrenchRunAuto1(){
    CommandBase turnAndShoot = new TurnAndShootCommand(this, 30).withTimeout(4);
    CommandBase driveToTrench = trajectoryHandler.getPathDriveCommand("trench");
    CommandBase driveFromTrench = trajectoryHandler.getPathDriveCommand("trench_reverse");
    CommandBase openAndCollect = null;
    CommandBase finalCommand = turnAndShoot.andThen(driveToTrench.alongWith(openAndCollect)).andThen(driveFromTrench.alongWith(closeCollectorCommand)).andThen(turnAndShoot);
    return finalCommand;
  }

  public CommandBase getTrenchRunAuto2(){
    CommandBase turnAndShoot = new TurnAndShootCommand(this, 30).withTimeout(4);
    CommandBase driveToTrench = trajectoryHandler.getPathDriveCommand("trench");
    CommandBase driveFromTrench = trajectoryHandler.getPathDriveCommand("trench_reverse2");
    CommandBase openAndCollect = null;
    CommandBase finalCommand = turnAndShoot.andThen(driveToTrench.alongWith(openAndCollect)).andThen(driveFromTrench.alongWith(closeCollectorCommand)).andThen(turnAndShoot);
    return finalCommand;
  }

  public CommandBase getCenterAuto(){
    CommandBase turnAndShoot = new TurnAndShootCommand(this, 30).withTimeout(4);
    CommandBase driveToClimb = trajectoryHandler.getPathDriveCommand("climb");
    CommandBase turn90Deg = new TurnInPlaceCommand(drivingSubsystem, 90);
    CommandBase straitDrive = new DriveByMetersCommand(drivingSubsystem, 1);

    CommandBase openAndCollect = null;
    CommandBase finalCommand = turnAndShoot.andThen(driveToClimb.alongWith(openAndCollect)).andThen(turn90Deg.alongWith(closeCollectorCommand)).andThen(straitDrive);
    return finalCommand;
  }

  public CommandBase getOpponentTrenchAuto(){
    CommandBase turnAndShoot = new TurnAndShootCommand(this, 30).withTimeout(4);

    CommandBase straitDriveToTrench = new DriveByMetersCommand(drivingSubsystem, 3);
    CommandBase straitDriveFromTrench = new DriveByMetersCommand(drivingSubsystem, -0.5);
    CommandBase turn90Deg = new TurnInPlaceCommand(drivingSubsystem, 90);

    CommandBase driveToShoot = trajectoryHandler.getPathDriveCommand("tower");

    CommandBase openAndCollect = null;
    CommandBase finalCommand = straitDriveToTrench.alongWith(openAndCollect).andThen(straitDriveFromTrench.alongWith(closeCollectorCommand).andThen(turn90Deg)).andThen(driveToShoot).andThen(turnAndShoot);
    return finalCommand;
  }
}
