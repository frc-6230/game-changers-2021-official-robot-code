package frc.robot.Utilities;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.LimelightUtil;
import frc.robot.Constants;

public class Vision {
    static double gyro = 0;
    static boolean canShoot;
    static double shootAngle;

    //implement real gyro

    public void procces(double angle){

        ShootingPrefernces();

        //gyro = NetworkTableInstance.getDefault().getTable("koip").getEntry("cameraAngle").getDouble(0);
        gyro = angle;
        SmartDashboard.putBoolean("Inner Turn", CanShootToInnerHole(getMainPredictedSlope()));//2

        SmartDashboard.putBoolean("Inner No Turn", CanShootToInnerHole(getMainSlope()));//1
        SmartDashboard.putNumber("outer", getShootAngle());// 1
        SmartDashboard.putBoolean("Outer Turn", CanShootToOuterHole(getMainPredictedSlope()));//4

        SmartDashboard.putBoolean("Outer No Turn", CanShootToOuterHole(getMainSlope()));//3

        SmartDashboard.putNumber("Distance", getDistance());

        SmartDashboard.putNumber("xAxis", getTargetxAxisDistance());
        SmartDashboard.putNumber("yAxis", getTargetyAxisDistance());
        SmartDashboard.putNumber("b1", getB1(getMainPredictedSlope()));
        SmartDashboard.putNumber("b2", getB2(getMainPredictedSlope()));

        SmartDashboard.putNumber("predicted Sloper", getMainPredictedSlope());
    }
    /**
     * 
     * @return Tx from the limelightUtil
     */
    public static double getTx(){
        return LimelightUtil.getdegRotationToTarget();
    }

    public static double getTargetyAxisDistance(){
        double distance =getDistance();
        return distance*Math.sin(Math.toRadians(90 - gyro - getTx()));
    }

    public static double getMaxInnerHoleEntryPoint(){
        return getTargetxAxisDistance() + Constants.FieldConstants.INNER_HOLE_RADIUS;

    }

    public static double getMinInnerHoleEntryPoint(){
        return getTargetxAxisDistance() - Constants.FieldConstants.INNER_HOLE_RADIUS;
    }


    public static double getMaxOuterHoleEntryPoint(){
        return getTargetxAxisDistance() + Constants.FieldConstants.OUTER_HOLE_RADIUS;

    }

    public static  double getMinOuterHoleEntryPoint(){
        return getTargetxAxisDistance() - Constants.FieldConstants.OUTER_HOLE_RADIUS;
    }
    /**
     * getTargetyAxisDistance()+Constants.FieldConstants.DISTANCE_BETWEEN_INNER_HOLE_AND_OUTER_HOLE is BP
     * 
     * getTargetxAxisDistance() is PR
     * 
     * @return  BRP angle
     * 
     * get mainslope,   y= mx+b
     */
    public static  double  getMainPredictedSlope(){
        return (getTargetyAxisDistance()+Constants.FieldConstants.DISTANCE_BETWEEN_INNER_HOLE_AND_OUTER_HOLE)/getTargetxAxisDistance(); //BRP SLOPE
        
    }
    /**
     * Math.atan makes the slope an angle
     * 
     * 
     * 
     * @return BRP angle in radians
     */
    public static  double getMainPredictedAngle(){
        return Math.atan(getMainPredictedSlope());
    }


    public static double getMainSlope(){
        return Math.tan(Math.toRadians(90-gyro));
    }


    public static double getDistance(){
        double ty =LimelightUtil.getVerticalRotationToTarget();
        return ty*ty*Constants.VisionConstants.kA + ty*Constants.VisionConstants.kB + Constants.VisionConstants.kC;

    }

    public static double getTargetxAxisDistance(){
        return getDistance()*Math.cos(Math.toRadians(90 - gyro - getTx())); //RP

    }

    public static double getB1(double mainSlope){
        return Constants.FieldConstants.BALL_RADIUS*Math.sqrt(mainSlope*mainSlope+1); // get y intersect

    }

    public static double getB2(double mainSlope){
        return -getB1(mainSlope);
    }

    public static double getInnerYaxis(){
        return getTargetyAxisDistance()+Constants.FieldConstants.DISTANCE_BETWEEN_INNER_HOLE_AND_OUTER_HOLE;
    }
    /**
     * checks if the ball is inside the target proportions to get to inner hole
     * @param mainSlope 
     * @return Boolean whether the ball can go in inner hole
     */
    public static boolean CanShootToInnerHole(double mainSlope){

        double gx = (getInnerYaxis()-getB1(mainSlope))/mainSlope;
        double hx = (getInnerYaxis()-getB2(mainSlope))/mainSlope;

        SmartDashboard.putNumber("hx", hx);
        SmartDashboard.putNumber("gx", gx);

        return getMinInnerHoleEntryPoint() <= gx && gx <= getMaxInnerHoleEntryPoint() && getMinInnerHoleEntryPoint() <= hx && hx <= getMaxInnerHoleEntryPoint();

    }
    /**
     * checks if the ball is inside the target proportions to get to outer hole
     * @param mainSlope
     * @return Boolean whether the ball can go in outer hole
     */
    public static boolean CanShootToOuterHole(double mainSlope){
        double gx = (getTargetyAxisDistance()-getB1(mainSlope))/mainSlope;
        double hx = (getTargetyAxisDistance()-getB2(mainSlope))/mainSlope;

        SmartDashboard.putNumber("hx", hx);
        SmartDashboard.putNumber("gx", gx);
 
        return getMinOuterHoleEntryPoint() <= gx && gx <= getMaxOuterHoleEntryPoint() && getMinOuterHoleEntryPoint() <= hx && hx <= getMaxOuterHoleEntryPoint();
    }
    
    /**
     * checks preferences for shooting and which angle to shoot from.
     * 
     */
    public static void ShootingPrefernces(){
        boolean outerTurn=CanShootToOuterHole(getMainPredictedSlope());
        boolean outerNoTurn = CanShootToOuterHole(getMainSlope());
        boolean innerNoTurn =CanShootToInnerHole(getMainSlope());
        boolean innerTurn = CanShootToInnerHole(getMainPredictedSlope());


        if(outerTurn && innerTurn){
            canShoot = true;
            shootAngle = 90 - gyro - Math.toDegrees(getMainPredictedAngle());
        }
        else if(outerNoTurn && innerNoTurn){
            canShoot = true;
            shootAngle = 0;
        }
        else{
            canShoot = false;
            
        }
        
    }

    public  static double getShootAngle(){
       return shootAngle;   
    }

    public static int getRpm(){
        return 0; //needs to be changed by  קו מגמה
    }
        
}

