// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {

    public static final class PWM{
        public static final int DRIVING_LEFT_VICTOR = 0;
        public static final int DRIVING_RIGHT_VICTOR = 1;
        public static final int COLLECT_VICTOR = 5;
        public static final int SHOOTING_VICTOR = 1;
        public static final int ANGLE_CHANGER_VICTOR =2;
    }

    public static final class DIO{
        public static final int[] DRIVING_LEFT_ENCODER = {0,0};
        public static final int[] DRIVING_RIGHT_ENCODER = {0,0};
        public static final int[] SHOOTING_ENCODER = {0,1};
        public static final int[] ANGLE_CHANGER_ENCODER = {0,2};
    }

    public static final class DriveConstants{

		public static final double ksVolts = 0;
		public static final double kvVoltSecondsPerMeter = 0;
		public static final double kaVoltSecondsSquaredPerMeter = 0;
        public static final double kPDriveVel = 0;
        public static final double MAX_VOLTAGE = 10;

    }

    public static final class AutoConstants{

		public static final double kRamseteB = 0;
		public static final double kRamseteZeta = 0;

    }


    public static final class RobotConstants{

        public static final double WHEEL_DIAMETER = 3; // meters
        public static final double WHEEL_PERIMETER = Math.PI * WHEEL_DIAMETER; // meters
        public static final double REV_ENCODER_TICKS = 2048; // ticks
        public static final double ANGLE_SHOOTER_PERIMETER = 13;
        public static final double ANGLE_SHOOTER_ARC_RADIUS = 26;
        public static final double FEEDER_SPEED = 983247;
        public static final double DELIVERY_ERSPEED = 98324347;

        public static final double ROBOT_WIDTH = 0.63; //meterss

    }

    public static final class PidConstants{

        public static final class TurnInPlace {
            public static final double kp = 6;
            public static final double kI = 8;
            public static final double kD = 51;  
            public static final double TOLERANCE = 1;   
        }

        public static final class DriveByMeters {

            public static final double kP = 6;
            public static final double kI = 8;
            public static final double kD = 51;  
            public static final double TOLERANCE = 1; 
        }

    }

    
    public static final class ShooterConstants{
        public static final int ENCODER_TICKS = 2048;

        public static final class PIDF{
            public static final double kP = 0;
            public static final double kI = 0;
            public static final double kD = 0;
            public static final double kV = 0;
            public static final double kS = 0;
        }
        public static final class AngleChangerPID{
            public static final double kP = 0;
            public static final double kI = 0;
            public static final double kD = 0;

        }
    }

    public static final class PCM{
        public static final int[] COLLECT_PISTON = {0,1};
    }
    
    public final class FieldConstants{
        public static final double BALL_RADIUS = 0.2;
        public static final double INNER_HOLE_RADIUS = 0.3;
        public static final double OUTER_HOLE_RADIUS = 0.6;
        public static final double DISTANCE_BETWEEN_INNER_HOLE_AND_OUTER_HOLE = 0.3;
    }

    public final class VisionConstants{
        public static final double kA = 0.0071;
        public static final double kB = 0.4223;
        public static final double kC = 11.163;
    }

    public final class JoystickConstants{
        public static final int JOYSTICK_BUTTON1 = 5;
        public static final int OPEN_AND_COLLECT_BUTTON = 3;
        public static final int LOAD_AND_SHOOT_BUTTON =56;
        public static final int LOAD_BALLS_BUTTON =6;
        public static final int SHOOT_BUTTON =8758924;

    }


}
