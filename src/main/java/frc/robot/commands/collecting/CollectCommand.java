// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.collecting;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CollectionSubsystem;

public class CollectCommand extends CommandBase {

  private CollectionSubsystem collectionSubsystem;

  public CollectCommand(CollectionSubsystem collectionSubsystem) {
    this.collectionSubsystem = collectionSubsystem;
    addRequirements(collectionSubsystem);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    collectionSubsystem.collect();
  }

  @Override
  public void end(boolean interrupted) {
    collectionSubsystem.stop();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
