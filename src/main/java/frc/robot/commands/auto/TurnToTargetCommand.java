// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Utilities.Vision;
import frc.robot.subsystems.DrivingSubsystem;

public class TurnToTargetCommand extends TurnInPlaceCommand {
  /** Creates a new TurnToTargetCommand. */
  public TurnToTargetCommand(DrivingSubsystem drivingSubsys) {
    super(drivingSubsys,0);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    this.angle = Vision.getShootAngle();
  }
}
