// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.RobotContainer;
import frc.robot.commands.shooting.ShootByAngle;


// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class TurnAndShootWithoutDelivery extends SequentialCommandGroup {
  /** Creates a new TurnAndShootWithoutDelivery. */
  public TurnAndShootWithoutDelivery(RobotContainer robotContainer,double angle) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(new TurnToTargetCommand(robotContainer.drivingSubsystem),new ShootByAngle(robotContainer, angle));
  }
}
