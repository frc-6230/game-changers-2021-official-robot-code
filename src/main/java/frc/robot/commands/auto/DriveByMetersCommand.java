// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.PidConstants.DriveByMeters;
import frc.robot.subsystems.DrivingSubsystem;

public class DriveByMetersCommand extends CommandBase {

  private double meters;
  private PIDController leftController, rightController;
  private DrivingSubsystem drivingSystem;

  public DriveByMetersCommand(DrivingSubsystem drivingSystem, double meters) {

    this.drivingSystem = drivingSystem;
    this.meters = meters;

    leftController = new PIDController(DriveByMeters.kP, DriveByMeters.kI, DriveByMeters.kD);
    rightController = new PIDController(DriveByMeters.kP, DriveByMeters.kI, DriveByMeters.kD);

    leftController.setTolerance(DriveByMeters.TOLERANCE);
    rightController.setTolerance(DriveByMeters.TOLERANCE);

    addRequirements(drivingSystem);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    leftController.setSetpoint(drivingSystem.getLeftenc().getDistance() + meters);
    rightController.setSetpoint(drivingSystem.getRightenc().getDistance() + meters);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double leftOutput = leftController.calculate(drivingSystem.getLeftenc().getDistance());
    double rightOutput = rightController.calculate(drivingSystem.getRightenc().getDistance());

    drivingSystem.driveVoltage(leftOutput, rightOutput);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drivingSystem.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return leftController.atSetpoint() && rightController.atSetpoint();
  }

}
