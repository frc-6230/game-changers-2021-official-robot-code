// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;


import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivingSubsystem;

import static frc.robot.Constants.PidConstants.TurnInPlace;

public class TurnInPlaceCommand extends CommandBase {

  private DrivingSubsystem drivingSubsys;
  private PIDController pidController;
  protected double angle;
  
  /** Creates a new TurnInPlaceCommand. */
  public TurnInPlaceCommand(DrivingSubsystem drivingSubsys,double angle) {

    this.drivingSubsys = drivingSubsys;
    this.pidController = new PIDController(TurnInPlace.kp,TurnInPlace.kI,TurnInPlace.kD);
    this.angle = angle;
    
    this.pidController.setTolerance(TurnInPlace.TOLERANCE);

  
    addRequirements(drivingSubsys);

    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    pidController.setSetpoint(drivingSubsys.getGyro().getAngle()+angle);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double speed = pidController.calculate(drivingSubsys.getGyro().getAngle());
    drivingSubsys.driveVoltage(speed,-speed);
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drivingSubsys.stop();
    
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return pidController.atSetpoint();
  }
}
