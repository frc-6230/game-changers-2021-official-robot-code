package frc.robot.commands.delivery;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DeliverySubsystem;

public class LoadBallsCommand extends CommandBase {

  private DeliverySubsystem deliverySystem;
 
  public LoadBallsCommand(DeliverySubsystem deliverysystem){
    this.deliverySystem = deliverysystem;
    addRequirements(deliverysystem);
  }
  
  @Override
  public void initialize() {}

  @Override
  public void execute() {
    deliverySystem.enableFeeder(0);
    deliverySystem.enableDelivery(0);
  }

  @Override
  public void end(boolean interrupted) {
    deliverySystem.enableFeeder(0);
    deliverySystem.enableDelivery(0);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
