// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooting;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Utilities.Vision;
import frc.robot.subsystems.ShootingSubsystem;

public class ShootByRpm extends CommandBase {
  protected ShootingSubsystem shootingSubsys;
  private int rpm;
  /** Creates a new ShootByRpm. */
  public ShootByRpm(ShootingSubsystem shootingSubsys) {
    this.shootingSubsys = shootingSubsys;
    addRequirements(shootingSubsys);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    this.rpm = Vision.getRpm();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    shootingSubsys.enableRPM(rpm);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
