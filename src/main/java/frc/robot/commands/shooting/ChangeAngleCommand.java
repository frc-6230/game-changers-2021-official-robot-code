// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooting;

import edu.wpi.first.wpilibj.controller.PIDController;
import frc.robot.Constants.ShooterConstants.AngleChangerPID;
import frc.robot.subsystems.AngleChangerSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class ChangeAngleCommand extends CommandBase {
  private PIDController pidController;
  private AngleChangerSubsystem angleChangerSubsys;
  private double angle;


  /** Creates a new ChangeAngle. */
  public ChangeAngleCommand(AngleChangerSubsystem angleChangerSubsys,double angle) {
    this.angle = angle;
    this.angleChangerSubsys = angleChangerSubsys;
    pidController = new PIDController(AngleChangerPID.kP,AngleChangerPID.kI,AngleChangerPID.kD);
    addRequirements(angleChangerSubsys);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    pidController.setSetpoint(angle);
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double rotationSpeed  =pidController.calculate(angleChangerSubsys.getEncAngle());
    angleChangerSubsys.set(rotationSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    angleChangerSubsys.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return pidController.atSetpoint();
  }
}
