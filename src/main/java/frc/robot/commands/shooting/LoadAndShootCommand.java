// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooting;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DeliverySubsystem;
import frc.robot.subsystems.ShootingSubsystem;
import frc.robot.Constants.RobotConstants;
import frc.robot.Utilities.Vision;

public class LoadAndShootCommand extends ShootToTargetCommand {
  private DeliverySubsystem deliverysubsys;
  /** Creates a new LoadAndShootCommand. */
  public LoadAndShootCommand(ShootingSubsystem shootingsubsys,DeliverySubsystem deliverysubsys) {
    super(shootingsubsys,0);
    this.deliverysubsys = deliverysubsys;
    

    addRequirements(deliverysubsys);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    rpm =Vision.getRpm();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    super.execute();
    if (shootingSubsys.isAtDesiredVolocity()){
      deliverysubsys.enableFeeder(RobotConstants.FEEDER_SPEED);
      deliverysubsys.enableDelivery(RobotConstants.DELIVERY_ERSPEED);
    }
    

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    deliverysubsys.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
