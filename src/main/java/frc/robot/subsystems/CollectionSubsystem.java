// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class CollectionSubsystem extends SubsystemBase {

  public static final double COLLECT_SPEED = 0.4; // 40% presents

  private VictorSP collectVictor;
  private DoubleSolenoid mainPiston;

  public CollectionSubsystem() {

    collectVictor = new VictorSP(Constants.PWM.COLLECT_VICTOR);

    mainPiston = new DoubleSolenoid(Constants.PCM.COLLECT_PISTON[0],Constants.PCM.COLLECT_PISTON[1]);

  }

  public void collect(){
    collectVictor.set(COLLECT_SPEED);
  }

  public void openCollector(){
    mainPiston.set(Value.kForward);
  }

  public void closeCollector(){
    mainPiston.set(Value.kReverse);
  }

  public void stop(){
    collectVictor.set(0);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
