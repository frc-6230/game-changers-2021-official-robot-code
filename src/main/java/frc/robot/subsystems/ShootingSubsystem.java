package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.ShooterConstants;
import frc.robot.Constants.ShooterConstants.PIDF;

public class ShootingSubsystem extends SubsystemBase {

  private VictorSP mainVictor;
  private Encoder mainEncoder;

  private PIDController pidController;
  private SimpleMotorFeedforward feedforward;

  public ShootingSubsystem(){

    mainVictor = new VictorSP(Constants.PWM.SHOOTING_VICTOR);

    mainEncoder = new Encoder(Constants.DIO.SHOOTING_ENCODER[0], Constants.DIO.SHOOTING_ENCODER[1]);

    mainEncoder.setDistancePerPulse(1.0/ShooterConstants.ENCODER_TICKS); // rpm

    initializeControl();
  }

  private void initializeControl(){

    pidController = new PIDController(PIDF.kP, PIDF.kI, PIDF.kD);

    feedforward = new SimpleMotorFeedforward(PIDF.kS, PIDF.kV);

  }

  public boolean isAtDesiredVolocity(){
    return pidController.atSetpoint();
  }

  public void initializeControl(double p, double i, double d, double s, double v){

    pidController.setPID(p,i,d);

    feedforward = new SimpleMotorFeedforward(s, v);

  }

  public void enableRPM(int desiredRPM){

    double output = feedforward.calculate(desiredRPM);

    double currentRpmSpeed = mainEncoder.getRate()*60;

    SmartDashboard.putNumber("RPM SPEED", currentRpmSpeed);

    SmartDashboard.putNumber("feedForward Output", output);
    
    output += pidController.calculate(currentRpmSpeed, desiredRPM);
    SmartDashboard.putNumber("PID", output);

    enable(output);

  }

  public void enable(double voltage){
    mainVictor.setVoltage(voltage);
  }

  public void stop(){
    enable(0);
  }
}