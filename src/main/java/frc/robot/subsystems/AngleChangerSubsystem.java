// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;

public class AngleChangerSubsystem extends SubsystemBase {
  private VictorSP angleChangerVictor;
  private Encoder angleChangerEncoder;
  
  /** Creates a new AngleChangerSubsystem. */
  public AngleChangerSubsystem() {
    angleChangerVictor = new VictorSP(Constants.PWM.ANGLE_CHANGER_VICTOR);
    angleChangerEncoder = new Encoder(Constants.DIO.ANGLE_CHANGER_ENCODER[0], Constants.DIO.ANGLE_CHANGER_ENCODER[1]);
    angleChangerEncoder.setDistancePerPulse((Constants.RobotConstants.ANGLE_SHOOTER_PERIMETER/Constants.ShooterConstants.ENCODER_TICKS)/Constants.RobotConstants.ANGLE_SHOOTER_ARC_RADIUS); // rpm
  }

  public double getEncAngle(){
    return Math.toDegrees(angleChangerEncoder.getDistance());
  }

  public void set(double voltage){
    angleChangerVictor.setVoltage(voltage);
  }
  public void stop(){
    set(0);
  }
  @Override
  public void periodic() {
    angleChangerEncoder.getDistance();
    // This method will be called once per scheduler run
  }
}
