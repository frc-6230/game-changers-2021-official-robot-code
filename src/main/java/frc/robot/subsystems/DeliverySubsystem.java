package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class DeliverySubsystem extends SubsystemBase {
    private VictorSP feederVictor; //monch
    private VictorSP deliveryVictor; //bleh  
 
  public DeliverySubsystem() {
    feederVictor = new VictorSP(0);
    deliveryVictor = new VictorSP(0);
    

  }
  public void enableFeeder(double speed){  //TheMoncher
    feederVictor.set(speed);
  }
  public void enableDelivery(double speed){  //TheBleher
    deliveryVictor.set(speed);
  }
  public void stop(){
    feederVictor.set(0);
    deliveryVictor.set(0);
  }
}
