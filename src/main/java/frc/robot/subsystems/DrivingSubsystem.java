
package frc.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.RobotConstants;

public class DrivingSubsystem extends SubsystemBase {

  private VictorSP rightVictor;
  private VictorSP leftVictor;
  private AHRS gyro;
  private Encoder leftenc;
  private Encoder rightenc;

  private DifferentialDrive differentialDrive;
  private DifferentialDriveOdometry differentialDriveOdometry;
  private DifferentialDriveKinematics driveKinematics;
  

  public DrivingSubsystem() {

    rightVictor = new VictorSP(Constants.PWM.DRIVING_RIGHT_VICTOR);
    leftVictor = new VictorSP(Constants.PWM.DRIVING_LEFT_VICTOR);

    leftenc = new Encoder(Constants.DIO.DRIVING_LEFT_ENCODER[0], Constants.DIO.DRIVING_LEFT_ENCODER[1]);
    rightenc = new Encoder(Constants.DIO.DRIVING_RIGHT_ENCODER[0], Constants.DIO.DRIVING_RIGHT_ENCODER[1]);

    leftenc.setDistancePerPulse(Constants.RobotConstants.WHEEL_PERIMETER/Constants.RobotConstants.REV_ENCODER_TICKS);
    rightenc.setDistancePerPulse(leftenc.getDistancePerPulse());
    
    leftVictor.setInverted(true);
    leftVictor.setInverted(true);

    gyro = new AHRS(Port.kOnboardCS1);

    resetEncoders();

    differentialDrive = new DifferentialDrive(leftVictor, rightVictor);
    differentialDriveOdometry = new DifferentialDriveOdometry(gyro.getRotation2d());
    driveKinematics = new DifferentialDriveKinematics(RobotConstants.ROBOT_WIDTH);

  }

  public void driveVoltage(double left, double right){
    leftVictor.setVoltage(left);
    rightVictor.setVoltage(right);
    differentialDrive.feed();
  }

  public void drivePresentage(double left, double right){
    leftVictor.set(left);
    rightVictor.set(right);
  }

  public void resetEncoders(){
    leftenc.reset();
    rightenc.reset();
  }

  public void stop(){
    drivePresentage(0, 0);
  }

  public Encoder getLeftenc() {
    return leftenc;
  }

  public Encoder getRightenc() {
    return rightenc;
  }

  public AHRS getGyro() {
    return gyro;
  }

  public DifferentialDriveKinematics getDriveKinematics() {
    return driveKinematics;
  }

  public Pose2d getPose() {
    return differentialDriveOdometry.getPoseMeters();
  }

  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(leftenc.getRate(), rightenc.getRate());
  }

  public void resetOdometry(Pose2d pose) {
    resetEncoders();
    differentialDriveOdometry.resetPosition(pose, gyro.getRotation2d());
  }

  public double getAverageEncoderDistance() {
    return (leftenc.getDistance() + rightenc.getDistance()) / 2.0;
  }

  public double getHeading() {
    return gyro.getRotation2d().getDegrees();
  }

  public double getTurnRate() {
    return -gyro.getRate();
  }

  @Override
  public void periodic() {
    differentialDriveOdometry.update(gyro.getRotation2d(), leftenc.getDistance(), rightenc.getDistance());
  }
}
